# Can I Fly There?

Can I Fly There is a WebExtensions browser plugin for FSEconomy that shows you if you can fly to an
airport in your simulator.

## Building

```
npm install
npm run build
```

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
